# nuke
💥 erase everything related to privacy concerns in a machine. Like browser cookies and your ssh key.

### to do
 - remove itself after running
 - check for other possible cache/storage locations from browsers
 - add a `.gitignore` file
 - add a license file

